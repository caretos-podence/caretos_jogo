﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    public HeadLookController HeadController;
    private Vector3 mousePos;
    private Vector3 Target;
    private Camera MainCamera;
    void Start()
    {
        MainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {

        mousePos = Input.mousePosition;
        Target = MainCamera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, MainCamera.transform.position.z));
        print(Target);
        HeadController.target = Target;
    }
}
